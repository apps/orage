/*
 * Copyright (c) 2021-2024 Erkki Moorits
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 *     Free Software Foundation
 *     51 Franklin Street, 5th Floor
 *     Boston, MA 02110-1301 USA
 */

#ifndef ORAGE_CSS_H
#define ORAGE_CSS_H 1

#define ORAGE_WEEK_WINDOW_SEPARATOR_BAR "hour-line"
#define ORAGE_WEEK_WINDOW_OCCUPIED_HOUR_LINE "hour-line-occupied"
#define ORAGE_WEEK_WINDOW_ODD_HOURS "odd-hours"
#define ORAGE_WEEK_WINDOW_EVEN_HOURS "even-hours"
#define ORAGE_WEEK_WINDOW_ALL_DAY_EVENT "all-day-event"
#define ORAGE_WEEK_WINDOW_TODAY "today"
#define ORAGE_WEEK_WINDOW_SUNDAY "sunday"
#define ORAGE_TODO_COMPLETED "todo-completed"
#define ORAGE_TODO_ACTUAL_NOW "todo-actual-now"

extern void orage_css_set_theme (void);

#endif
